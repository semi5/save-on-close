export function setup(ctx) {
    const id = 'save-on-close';
    const title = 'Save On Close';
    const desc = "This script automatically cloud saves when leaving the page so you don't forget";
    const imgSrc = 'assets/media/bank/rune_mind.png';

    const forceCloudSave = (event) => {
        forceSync(false, false);
        event.returnValue = `Wait for cloud save to complete.`;
    };

    ctx.onInterfaceReady(() => {
        window.addEventListener('beforeunload', forceCloudSave);
        mod.api.SEMI.log(id, 'Mod loaded successfully')
    })
}